# Copyright (c) 2020-2023 Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

set(CMAKE_ASM_FLAGS_INIT "-mthumb")
set(CMAKE_C_FLAGS_INIT "-mthumb -fomit-frame-pointer -funsigned-char")
set(CMAKE_CXX_FLAGS_INIT "-mthumb -fomit-frame-pointer -fno-exceptions -fno-rtti")

set(IOTSDK_TOOLCHAIN_ELIMINATE_UNUSED_SECTIONS ON CACHE BOOL
    "Eliminate unused sections to decrease binary size.")

# Quoted from https://gcc.gnu.org/onlinedocs/gcc/Optimize-Options.html
#
# -ffunction-sections -fdata-sections
#
# Only use these options when there are significant benefits from doing so. When
# you specify these options, the assembler and linker create larger object and
# executable files and are also slower. These options affect code generation.
# They prevent optimizations by the compiler and assembler using relative
# locations inside a translation unit since the locations are unknown until link
# time. An example of such an optimization is relaxing calls to short call
# instructions.
if(IOTSDK_TOOLCHAIN_ELIMINATE_UNUSED_SECTIONS)
    set(CMAKE_ASM_FLAGS_INIT "${CMAKE_ASM_FLAGS_INIT} -ffunction-sections -fdata-sections")
    set(CMAKE_C_FLAGS_INIT "${CMAKE_C_FLAGS_INIT} -ffunction-sections -fdata-sections")
    set(CMAKE_CXX_FLAGS_INIT "${CMAKE_CXX_FLAGS_INIT} -ffunction-sections -fdata-sections")
endif()

# Clear toolchain options for all languages as IOTSDK uses different
# initialization options (such as for optimization and debug symbols)
set(CMAKE_ASM_FLAGS_DEBUG "" CACHE STRING "" FORCE)
set(CMAKE_ASM_FLAGS_RELEASE "" CACHE STRING "" FORCE)
set(CMAKE_ASM_FLAGS_RELWITHDEBINFO "" CACHE STRING "" FORCE)
set(CMAKE_C_FLAGS_DEBUG "" CACHE STRING "" FORCE)
set(CMAKE_C_FLAGS_RELEASE "" CACHE STRING "" FORCE)
set(CMAKE_C_FLAGS_RELWITHDEBINFO "" CACHE STRING "" FORCE)
set(CMAKE_CXX_FLAGS_DEBUG "" CACHE STRING "" FORCE)
set(CMAKE_CXX_FLAGS_RELEASE "" CACHE STRING "" FORCE)
set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "" CACHE STRING "" FORCE)
